from pydantic import BaseModel
import csv


class Person(BaseModel):
    name: str
    age: int
    email: str


# Create some instances
people = [
    Person(name="Alice", age=30, email="alice@example.com"),
    Person(name="Bob", age=40, email="bob@example.com")
]


# Define the header
fields = ["Label", "Value"]

# Name of the output csv file
filename = "people.csv"

# Writing to csv file
with open(filename, 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(fields)  # Write header

    for person in people:
        for label, value in person.model_dump().items():
            csvwriter.writerow([label, value])
